#include <iostream>
#include <vector>
#include <fstream>
#include <experimental/filesystem>

#include <jsoncpp/json/json.h>

namespace fs = std::experimental::filesystem;

std::vector<std::string> split(const std::string &s, char delim) 
{
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
  
	while (std::getline(ss, item, delim))
		elems.push_back(item);
	
	return elems;
}

int findInVector(std::vector<std::string> vec, std::string pattern)
{		
	for (int i = 0; i < vec.size(); i++)
		if (vec[i] == pattern)
			return true;
	
	return false;
}

int main()
{	
	std::string fstorage = "CoreScripts/data/RealEstate/storage.json";
	
	try
	{   
		Json::Value root;
		std::ifstream storage(fstorage, std::ifstream::binary);
		storage >> root;
		std::vector<std::string> keys = root.getMemberNames();
		
		std::vector<std::string> houses;
		
		for(auto house : keys)
			if (!root[house].isArray())
				houses.push_back(house);
		
		for (auto & p : fs::directory_iterator("CoreScripts/data/cell"))
		{
			std::string path = p.path();
			path.erase(0, 22);
			path = split(path, '.')[0];
			
			if (findInVector(houses, path))
			{
				std::cout << path << " has owner, skip" << std::endl;
				continue;
			}
			
			std::cout << "Remove " << path << std::endl;
			fs::remove(p.path());
		}
    }
    catch(std::exception& e) 
    {
       std::cout << "error " << e.what() << std::endl;
    }

	return 0;
}
