# tes3mp Server Tools
Helpers for tes3mp linux servers.

### tes3mp-launch

Restart server if it's crash. For exit from server press ctrl+c.

Requirements: sh

### cell-cleaner.py

Backup all cells in $HOME/backups/$(date)/cells and all players in $HOME/backups/$(date)/players; Then clear all cells, exept of RealEstate cells.

Requirements: python 3

### server-deploy 

OUTDATED. Use https://gitlab.com/freedomland/openmw-tes3mp build instruction instead.

Deploy minimal server possible. 

### TES3MP-Deploy-fork

OUTDATED. Use https://gitlab.com/freedomland/openmw-tes3mp build instruction instead.

### cell-cleaner

OUTDATED. Use cell-cleaner.py instead

### cleanup.cpp

OUTDATED. Use cell-cleaner.py instead

### License
tes3mp Server Tools (c) Volk_Milit, GPL v3.0.
