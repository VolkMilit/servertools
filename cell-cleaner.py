import json, os, getpass, datetime
import shutil
from pathlib import Path

ignore_list = []

if not os.path.exists('/home/{}/backups'.format(getpass.getuser())):
    os.makedirs('/home/{}/backups'.format(getpass.getuser()))
    
date = datetime.datetime.now()
os.mkdir('/home/{}/backups/{}'.format(getpass.getuser(), date))

shutil.copytree('CoreScripts/data/cell', '/home/{}/backups/{}/cell'.format(getpass.getuser(), date))
shutil.copytree('CoreScripts/data/player', '/home/{}/backups/{}/player'.format(getpass.getuser(), date))

def append_to_ignore(file_name):
	with open(file_name) as f:
		d = json.load(f)
		for key, value in d.items():
			# check dictionary, if we had list, this mean this house
			# was sold or something and we should ignore it
			if type(value) == list:
				continue
			else:
				# replace dot with comma, as tes3mp do the same
				# when cell saving
				key = key.replace('.', ',')
				ignore_list.append(key + '.json')
				print('"' + key + '" has owner, ignore')
				
		f.close()
	
append_to_ignore('CoreScripts/data/RealEstate/storage.json')
append_to_ignore('CoreScripts/data/RealEstate/rent.json')

pathlist = Path().glob('CoreScripts/data/cell/*.json')
for path in pathlist:
	if not os.path.basename(path) in ignore_list:
		os.remove(path)
		print('Clear "' + os.path.basename(path) + '"')
